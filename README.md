Assignment 28 March 2020

Need optimize solution for overall speed and performance.
As optimization technique you can use:
- caching
- choosing proper data structures
- eliminatiing results.

For measuring time consider to use following approach:
https://docs.python.org/2/library/timeit.html

Final results need to be populated at https://docs.google.com/spreadsheets/d/1g0tmHHnHemLkRkdw4OQ9nMXF6G9R7eLNKyXeSNudNz0/edit#gid=0


Use should download this spreadsheet and link to the bsu.mmf.it@gmail.com.
Questions should be sent at bsu.mmf.it@gmail.com
